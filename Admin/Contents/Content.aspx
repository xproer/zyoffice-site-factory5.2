﻿<%@ Page Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true"
    Inherits="PowerEasy.Module.General.WebSite.Admin.Contents.Content" Title="内容添加/修改"
    ValidateRequest="false" EnableEventValidation="false" CodeBehind="Content.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CphNavigation" runat="Server">
    <table style="width: 100%; margin: 0 auto;" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <pe:ExtendedSiteMapPath ID="SmpNavigator" SiteMapProvider="AdminMapProvider" runat="server" />
            </td>
            <td align="right">
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CphContent" runat="Server">
    <asp:ScriptManager ID="SmContent" runat="server" ScriptMode="Release" EnablePartialRendering="true">
    </asp:ScriptManager>
    <!--zyOffice begin-->    
    <link type="text/css" rel="Stylesheet" href="/zyoffice/js/skygqbox.css" />
    <script type="text/javascript" src="/zyoffice/js/json2.min.js" charset="utf-8"></script>    
    <script type="text/javascript" src="/zyoffice/js/skygqbox.js" charset="utf-8"></script>
    <script type="text/javascript" src="/zyoffice/js/w.js" charset="utf-8"></script>
    <!--zyOffice end-->

    <script language="JavaScript" type="text/JavaScript">
   var tID=0;
   var arrTabTitle = new Array("TabTitle0","TabTitle1","<%= TabTitle2.ClientID %>","<%= TabTitle3.ClientID %>","<%=TabTitle4.ClientID %>","<%=TabTitle5.ClientID %>","<%=TabTitle6.ClientID %>");
   var arrTrs0 = new Array(<%= arrTrs0 %>);
   var arrTrs1 = new Array(<%= arrTrs1 %>);
   var arrTrs2 = new Array("TabsCharge"<%= m_TbodyChargeId %>);
   var arrTrs3 = new Array("TabsSign");
   var arrTrs4 = new Array("TabsVote");
   var arrTrs5 = new Array("TabsCorrelativeItem");
   var arrTrs6 = new Array("TabsWapInfo");
   var arrTab = new Array(arrTrs0,arrTrs1,arrTrs2,arrTrs3,arrTrs4,arrTrs5,arrTrs6);
    //zyOffice开始
    zyOffice.getInstance({});//加载控件
    
    //zyOffice结束
   function ShowTabs(ID)
   {
       if(ID!=tID)
       {
           document.getElementById(arrTabTitle[tID].toString()).className = "tabtitle";
           document.getElementById(arrTabTitle[ID].toString()).className = "titlemouseover";
           
           for (var i = 0; i < arrTab.length; i++) 
           {
                var tab = arrTab[i];
                if(i==ID)
                {
                    for (var j = 0; j < tab.length; j++) 
                    {
                       document.getElementById(tab[j].toString()).style.display = "";
                    }
                }
                else
                {
                    for (var j = 0; j < tab.length; j++) 
                    {
                       document.getElementById(tab[j].toString()).style.display = "none";
                    }
                }
           }
           
           tID=ID;
        }
    }
    
    function ClearUser()
    {
        document.getElementById(<%="'" +TxtSigninUser.ClientID + "'" %>).value="";
    }
    </script>

    <div style="padding-top: 5px;">
    </div>
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr align="center">
            <td id="TabTitle0" class="titlemouseover" onclick="ShowTabs(0)">
                基本信息
            </td>
            <td id="TabTitle1" class="tabtitle" onclick="ShowTabs(1)">
                属性选项
            </td>
            <td id="TabTitle2" class="tabtitle" runat="server" onclick="ShowTabs(2)">
                权限收费设置
            </td>
            <td id="TabTitle3" class="tabtitle" runat="server" onclick="ShowTabs(3)">
                签收选项
            </td>
            <td id="TabTitle4" class="tabtitle" runat="server" onclick="ShowTabs(4)">
                投票
            </td>
            <td id="TabTitle5" class="tabtitle" runat="server" onclick="ShowTabs(5)">
                相关信息
            </td>
            <td id="TabTitle6" class="tabtitle" runat="server" onclick="ShowTabs(6)">
                WAP内容
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
    <table style="width: 100%; margin: 0 auto;" cellpadding="2" cellspacing="1" class="border">
        <pe:ExtendedAdminRepeater ID="ContentForm" runat="server" OnItemDataBound="ContentForm_OnItemDataBound">
            <ItemTemplate>
                <tr id='Tab' runat="server" class='tdbg'>
                    <td class='tdbgleft' align='right' style="width: 20%;">
                        <div class="DivWordBreak">
                            <strong>
                                <%# Eval("FieldAlias")%>
                                ：&nbsp;</strong><br />
                            <%# Eval("Tips") %>
                        </div>
                    </td>
                    <td class='tdbg' align='left'>
                        <pe:AdminFieldControl ID="Field" runat="server" EnableNull='<%# (bool)Eval("EnableNull") %>'
                            FieldAlias='<%# Eval("FieldAlias")%>' Tips='<%# Eval("Tips") %>' FieldName='<%#Eval("FieldName")%>'
                            ControlType='<%# Eval("FieldType") %>' FieldLevel='<%# Eval("FieldLevel") %>'
                            Description='<%# Eval("Description")%>' Settings='<%# ((PowerEasy.Module.General.Model.CommonModel.FieldInfo)Container.DataItem).Settings %>'
                            Value='<%# Eval("DefaultValue") %>' Disabled='<%# Eval("Disabled") %>'>
                        </pe:AdminFieldControl>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                <tr id='SiteGroupTab' runat="server" class='tdbg'>
                    <td class='tdbgleft' runat="server" ID="SiteGroupAddPh" Visible="false" align='right' style="width: 20%;">
                        <div class="DivWordBreak">
                            <strong>添加到站群系统： </strong>
                            <br />
                        </div>
                    </td>
                    <td class='tdbgleft' align='right' style="width: 20%;" runat="server" ID="SiteGroupEditPh" Visible="false">
                        <div class="DivWordBreak">
                            <strong>更新到站群系统： </strong>
                            <br />
                        </div>
                    </td>
                    <td class='tdbg' align='left'>
                        <asp:RadioButtonList ID="RadioSiteGroup" runat="server" RepeatColumns="2">
                            <asp:ListItem Value="true">是</asp:ListItem>
                            <asp:ListItem Value="false" Selected>否</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </FooterTemplate>
        </pe:ExtendedAdminRepeater>
        <tbody id="TabsCharge" style="display: none">
            <tr class="tdbg">
                <td style="width: 150px;" align="right" class="tdbgleft">
                    <strong>阅读权限：&nbsp;</strong>
                </td>
                <td>
                    <asp:RadioButtonList ID="RadlInfoPurview" runat="server">
                        <asp:ListItem Value="0" Selected="True">继承栏目权限（当所属栏目为认证栏目时，建议选择此项）</asp:ListItem>
                        <asp:ListItem Value="1">所有会员（想单独对某些信息进行阅读权限设置，可以选择此项）</asp:ListItem>
                        <asp:ListItem Value="2">指定会员组（想单独对某些信息进行阅读权限设置，可以选择此项）</asp:ListItem>
                    </asp:RadioButtonList>
                    <table>
                        <tr>
                            <td width="20">
                            </td>
                            <td>
                                <pe:ExtendedCheckBoxList ID="EChklUserGroupList" RepeatColumns="5" runat="server">
                                </pe:ExtendedCheckBoxList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr> 
        </tbody>
        <tbody id="TbodyCharge" runat="server" style="display: none">
            <tr class="tdbg">
                <td style="width: 150px;" align="right" class="tdbgleft">
                    <strong>消费<pe:ShowPointName ID="ShowPointName" runat="server" />数：&nbsp;</strong>
                </td>
                <td style="height: 17px">
                    <asp:TextBox ID="TxtInfoPoint" runat="server" Columns="5" MaxLength="4"></asp:TextBox>&nbsp;
                    <span style="color: #0000FF">如果<pe:ShowPointName ID="ShowPointName1" runat="server" />数大于0，则有权限的会员阅读此文章时将消耗相应<pe:ShowPointName
                        ID="ShowPointName2" runat="server" />数（设为9999时除外），游客将无法阅读此文章</span>
                </td>
            </tr>
            <tr class="tdbg">
                <td style="width: 150px;" align="right" class="tdbgleft">
                    <strong>重复收费：&nbsp;</strong>
                </td>
                <td>
                    <pec:ShowChargeType ID="ShowChargeType" runat="server" />
                </td>
            </tr>
            <tr class="tdbg">
                <td style="width: 150px;" align="right" class="tdbgleft">
                    <strong>分成比例：&nbsp;</strong>
                </td>
                <td>
                    <asp:TextBox ID="TxtDividePercent" runat="server" Columns="5" MaxLength="3"></asp:TextBox>%
                    &nbsp;<span style="color: #0000FF">如果比例大于0，则将按比例把向阅读者收取的<pe:ShowPointName ID="ShowPointName3"
                        runat="server" />数支付给录入者</span>
                </td>
            </tr>
        </tbody>
        <tbody id="TabsSign" style="display: none">
            <tr class="tdbg">
                <td class="tdbgleft" align="right" style="width: 150px;">
                    <strong>签收用户：&nbsp;</strong>
                </td>
                <td class="tdbg" align="left">
                    <asp:TextBox ID="TxtSigninUser" Columns="72" Rows="5" runat="server" TextMode="MultiLine"
                        Height="75"></asp:TextBox><br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<pec:FloatingDialog ID="FloatingDialog"
                        runat="server" ShowType="1" Width="600" Height="400" Name="选择用户" Skin="inputbutton"
                        Title="选择用户" />
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="button" class="inputbutton" value="清除用户" onclick="ClearUser()" />
                </td>
            </tr>
            <tr class="tdbg">
                <td class="tdbgleft" align="right" style="width: 150px;">
                    <strong>文档类型：&nbsp;</strong>
                </td>
                <td class="tdbg" align="left">
                    <asp:DropDownList ID="DrpSigninType" runat="server">
                        <asp:ListItem Text="公众文档" Value="EnableSignInPublic"></asp:ListItem>
                        <asp:ListItem Text="专属文档" Value="EnableSignInPrivate"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="tdbg">
                <td class="tdbgleft" align="right" style="width: 150px;">
                    <strong>签收优先级：&nbsp;</strong><br />
                </td>
                <td class="tdbg" align="left">
                    <asp:TextBox ID="TxtPriority" runat="server" Columns="8"></asp:TextBox><pe:RegexValidator
                        ValidateType="Number" ID="Valnum" runat="server" ControlToValidate="TxtPriority"
                        ErrorMessage="请填写数字" SetFocusOnError="true" Display="Dynamic"></pe:RegexValidator><asp:RangeValidator
                            ID="RangeValTxtPriority" runat="server" Type="Integer" MaximumValue="2147483646"
                            MinimumValue="0" ControlToValidate="TxtPriority" Display="Dynamic" SetFocusOnError="true"
                            ErrorMessage="请填写0-2147483646范围的数字"></asp:RangeValidator>
                </td>
            </tr>
            <tr class="tdbg">
                <td class="tdbgleft" align="right" style="width: 150px;">
                    <strong>签收截止日期：&nbsp;</strong><br />
                </td>
                <td class="tdbg" align="left">
                    <pe:DatePicker ID="DpkEndTime" IsLongDate="true" runat="server"></pe:DatePicker>
                </td>
            </tr>
        </tbody>
        <tbody id="TabsVote" style="display: none">
            <pec:VoteControl ID="Vote" runat="server"></pec:VoteControl>
        </tbody>
        <tbody id="TabsCorrelativeItem" style="display: none">
            <tr>
                <td>
                    <pec:FloatingDialog ID="FloatingDialogCorrelativeItem" runat="server" ShowType="1"
                        Width="600" Height="400" Name="添加相关信息" Skin="inputbutton" Title="添加相关信息" />
                </td>
            </tr>
            <tr class="tdbg">
                <td valign="top">
                    <asp:UpdatePanel ID="panCorrelativeItem" ChildrenAsTriggers="false" runat="server"
                        UpdateMode="Conditional">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="RptCorrelativeItem" />
                            <asp:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
                        </Triggers>
                        <ContentTemplate>
                            <asp:HiddenField ID="HdnGeneralIds" runat="server" Value='' />
                            <asp:Repeater ID="RptCorrelativeItem" runat="server" OnItemDataBound="RptCorrelativeItem_ItemDataBound"
                                OnItemCommand="RptCorrelativeItem_ItemCommand">
                                <HeaderTemplate>
                                    <table width="100%" border="0" cellpadding="2" cellspacing="1" class="border">
                                        <tr align="center" class="title">
                                            <td style="width: 80%">
                                                名称/标题
                                            </td>
                                            <td>
                                                排序
                                            </td>
                                            <td style="width: 10%">
                                                操作
                                            </td>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class="tdbg">
                                        <td>
                                            <div class="linkType">
                                                <pe:ExtendedLabel ID="LblTitle" runat="server" HtmlEncode="false"></pe:ExtendedLabel>
                                            </div>
                                            <asp:HiddenField ID="GeneralId" runat="server" Value='<%# Eval("GeneralId")%>' />
                                            <asp:HiddenField ID="hdnModelId" Value='<%# Eval("ModelId")%>' runat="server" />
                                        </td>
                                        <td align="center">
                                            <asp:TextBox ID="TxtOrderId" Width="30" runat="server" Text='<%# Eval("OrderId")%>'></asp:TextBox>
                                        </td>
                                        <td align="center">
                                            <asp:LinkButton ID="LbtnDelete" runat="server" CommandName="DelOrderItem" CommandArgument='<%#Eval("GeneralId")%>'
                                                CausesValidation="False">删除</asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </tbody>
        <tbody id="TabsWapInfo" style="display: none">
            <tr>
                <td colspan="2">
                            <table style="width: 100%; margin: 0 auto;" cellpadding="2" cellspacing="1" class="border">
                                <tr class="tdbg">
                                    <td class="tdbgleft" align="right" style="width: 20%">
                                        <strong>获取数据：</strong>
                                    </td>
                                    <td class="tdbg" align="left">
                                        <asp:Button ID="btnSynchronous" ValidationGroup="2" runat="server" Text="获取" OnClick="btnSynchronous_Click" />
                                        <font style="color: Green;">从基本信息中获取信息</font>
                                    </td>
                                </tr>
                                <tr class="tdbg">
                                    <td class="tdbgleft" align="right" style="widows: 20%;">
                                        <strong>WAP类别：</strong>
                                    </td>
                                    <td class="tdbg" align="left">
                                        <asp:DropDownList ID="dropWapCategory" runat="server" DataTextField="WapCategoryName"
                                            DataValueField="WapCategoryID">
                                        </asp:DropDownList>
                                        <font style="color: Green;">如果选择了WAP类别则会将信息加入到WAP内容的相应类别中</font>
                                    </td>
                                </tr>
                                 <tr class="tdbg">
                                    <td class="tdbgleft" align="right" style="widows: 20%;">
                                        <strong>标题：</strong>
                                    </td>
                                    <td class="tdbg" align="left">
                                       <asp:TextBox ID="FieldTitle" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                 <tr class="tdbg">
                                    <td class="tdbgleft" align="right" style="widows: 20%;">
                                        <strong>作者：</strong>
                                    </td>
                                    <td class="tdbg" align="left">
                                       <asp:TextBox ID="FieldAuthor" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                 <tr class="tdbg">
                                    <td class="tdbgleft" align="right" style="widows: 20%;">
                                        <strong>内容：</strong>
                                    </td>
                                    <td class="tdbg" align="left">
                                       <pe:CKeditor ID="FieldContent" runat="server" Toolbar="Basic" Width="600"></pe:CKeditor>
                                    </td>
                                </tr>
                                 <tr class="tdbg">
                                    <td class="tdbgleft" align="right" style="widows: 20%;">
                                        <strong>状态：</strong>
                                    </td>
                                    <td class="tdbg" align="left">
                                        <asp:RadioButtonList ID="FieldStatus" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Text="草稿" Value="-1"></asp:ListItem>
                                            <asp:ListItem Text="待审核" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="终审通过" Value="99" Selected="True"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                </td>
            </tr>
        </tbody>
        <tr class="tdbgbottom">
            <td colspan="2">
                <pe:ExtendedButton ID="EBtnSubmit" IsChecked="false" IsShowTabs="true" CustomValProcessFunction="ValProcessFunction"
                    Text="保存添加的项目" OnClick="EBtnSubmit_Click" runat="server" OnClientClick="if(!ShowValidate()||!AddSiteGroupTip()){return false;}" />
                <asp:Button ID="BtnFactAddress" runat="server" Text="修改实项目" Visible="false" OnClick="BtnFactAddress_Click"
                    OnClientClick="if(!ShowValidate()||!AddSiteGroupTip()){return false;}" />
                <pe:ExtendedButton ID="EBtnNewAddItem" IsChecked="false" IsShowTabs="true" CustomValProcessFunction="ValProcessFunction"
                    Text="添加为新项目" runat="server" OnClick="EBtnNewAddItem_Click" Visible="false" OnClientClick="if(!ShowValidate()||!AddSiteGroupTip()){return false;}" />
                <asp:Button ID="BtnBack" runat="server" Text="返　回" OnClick="BtnBack_Click" UseSubmitBehavior="False"
                    CausesValidation="False" /><asp:Label ID="Label1" runat="server" Text="" Visible="false"></asp:Label>
            </td>
        </tr>
    </table>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
        ShowSummary="False" />
    <div style="display: none">
        <pe:OneClickButton ID="btnAdd" OnClick="btnAdd_Click" ValidationGroup="valsCorrelativeItem"
            runat="server" Text="确定" IsOneClick="false" />
    </div>

    <script type="text/javascript">
        function ValProcessFunction() {
            for (i = 0; i < Page_Validators.length; i++) {
                val = Page_Validators[i];
                if (val.isvalid == false) {
                    var id = val.id;
                    var controltovalidate = document.getElementById(val.controltovalidate);
                    var tempobj = controltovalidate;
                    var tabIndex;
                    while (tempobj) {
                        if (tempobj.id != "") {
                            if (tempobj.nodeName == "TR" || tempobj.nodeName == "TBODY") {
                                var tabIndex = -1;
                                for (var i = 0; i < arrTab.length; i++) {
                                    var tab = arrTab[i];
                                    for (var j = 0; j < tab.length; j++) {
                                        if (tempobj.id == tab[j].toString()) {
                                            tabIndex = i;
                                            break;
                                        }
                                    }
                                    if (tabIndex != -1) {
                                        break;
                                    }
                                }
                                ShowTabs(tabIndex);
                                break;
                            }
                        }
                        tempobj = tempobj.parentNode;
                    }
                    if (typeof (controltovalidate.focus) != "undefined" && controltovalidate.focus != null) {
                        try {
                            controltovalidate.focus();
                        }
                        catch (err) {
                        }
                    }
                    break;
                }
            }
        }
        function AddSiteGroupTip() {
            if ($("tr[id*='SiteGroupTab']").is(":visible")) {
                if ($("input[id*='RadioSiteGroup_0']").is(":checked")) {
                    return confirm('您选择了提交到站群系统，确定要提交吗？');
                }
            }
            return true;
        }

        function ShowValidate() {
            if (document.getElementById("<%= dropWapCategory.ClientID %>").value > 0) {
                if (document.getElementById("<%= FieldTitle.ClientID %>").value.length < 1) {
                    alert("请输入WAP标题!");
                    return false;
                }
                var flag = true;
                eval('if (<%= FieldContent.ClientID %>$$ckeditor$$obj.getData().length < 1) {flag=false; alert("请输入WAP内容!");}');
                return flag;
                
            }
            return true;
        }
    </script>

</asp:Content>
